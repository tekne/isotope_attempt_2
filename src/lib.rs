extern crate hashbrown;

pub mod ir;
pub mod typing;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
