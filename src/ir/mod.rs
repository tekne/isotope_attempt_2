use typed_generational_arena::{Arena, Index};
use hashbrown::hash_map::HashMap;

/// An SSA control flow graph, which given parameters, returns a set of output nodes via the ret
/// terminator, or nothing if it gets into an infinite loop. Note the set of ouput nodes can
/// be thought of as a single output node representing the set, which is how it is implemented.
/// Note parameters have no contextual information, however, the CFG can carry metadata.
/// Other structs *containing* a CFG (e.g. Module, Function, ...) need to give this!
pub struct CFG {
    parameters: Arena<Parameter>,
    values: Arena<Value>,
    blocks: Arena<Block>,
    metadata : CFGMetadata
}

impl CFG {
    /// Create a new, empty CFG (representing a noop)
    pub fn new() -> CFG {
        CFG {
            parameters : Arena::new(),
            values : Arena::new(),
            blocks : Arena::new(),
            metadata : CFGMetadata::new()
        }
    }
}

/// Metadata for a CFG
pub struct CFGMetadata {

}

impl CFGMetadata {
    /// Create a new empty container for CFG metadata
    pub fn new() -> CFGMetadata {
        CFGMetadata {}
    }
}

/// A parameter, which can be any value but is defined by the caller of a module.
pub struct Parameter {
    //TODO: this
}

/// A value (note all types are values, but not all values are types!)
pub struct Value {
    //TODO: this
}

/// A basic block
pub struct Block {
    //TODO: this
}
